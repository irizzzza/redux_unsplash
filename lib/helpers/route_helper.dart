import 'package:flutter/material.dart';
import 'package:redux_epics_practice/pages/home_page.dart';
import 'package:redux_epics_practice/pages/images_page.dart';
import 'package:redux_epics_practice/res/const.dart';

class RouteHelper {
  // region [Initialize]
  static const String TAG = '[RouteHelper]';
  RouteHelper._privateConstructor();
  static final RouteHelper _instance = RouteHelper._privateConstructor();
  static RouteHelper get instance => _instance;
  // endregion
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {


      case IMAGES_PAGE_ROUTE:
        return _defaultRoute(
          settings: settings,
          page: ImagesPage(),
        );
      case HOME_PAGE_ROUTE:
        return _defaultRoute(
          settings: settings,
          page: HomePage(),
        );
      default:
        return _defaultRoute(
          settings: settings,
          page: HomePage(),
        );
    }
  }
  static PageRoute _defaultRoute({@required RouteSettings settings, @required Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}