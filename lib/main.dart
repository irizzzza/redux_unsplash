import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux_epics_practice/app/my_app.dart';
import 'package:redux_epics_practice/store/appstate/appstate.dart';

void main() {

  Store store = Store<AppState>(AppState.reducer,
    initialState: AppState.initial(),
    middleware: [
      EpicMiddleware(AppState.getAppEpics),
    //  NavigationMiddleware<AppState>(),
    ]
  );

  runApp(MyApp(store));
}


