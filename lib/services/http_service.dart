import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:redux_epics_practice/models/base_network_response.dart';
import 'package:redux_epics_practice/res/const.dart';

enum HttpType {
  GET,
  PUT,
  POST,
  PATCH,
  DELETE,
}

class HttpService {

  // region [Initialize]
  static const String TAG = '[NetworkService]';

  HttpService._privateConstructor();

  static final HttpService _instance = HttpService._privateConstructor();

  static HttpService get instance => _instance;

  // endregion

  static const String TOKEN = EMPTY_STRING;

  Future<BaseNetworkResponse> request(
      String url, {
        HttpType type = HttpType.GET,
        Map<String, dynamic> headers,
        Map<String, dynamic> body,
        Map<String, dynamic> params,
        String token = EMPTY_STRING,
      }) async {


    if (token != null && token != EMPTY_STRING) headers = _addTokenToHeaders(headers, token);

    if (headers == null) {
      headers = {};
    }

    String bodyString = EMPTY_STRING;

    if (body != null) {
      bodyString = json.encode(body);

    }



    try {

      http.Response response;
      switch (type) {
        case HttpType.GET:
          response = await http.get(url);
          break;
        case HttpType.POST:
          response = await http.post(url, headers: headers, body: bodyString);
          break;
        case HttpType.PUT:
          response = await http.put(url, headers: headers, body: bodyString);
          break;
        case HttpType.PATCH:
          response = await http.patch(url, headers: headers);
          break;
        case HttpType.DELETE:
          response = await http.delete(url, headers: headers);
          break;
        default:
          response = await http.post(url, headers: headers, body: bodyString);
          break;
      }

      if (response.body == null || json.decode(response.body) == null) {
        return null;
      }

      return BaseNetworkResponse(
        response: json.decode(response.body),
      );
    } catch (error) {
      return null;
    }
  }
  Map<String, String> _addTokenToHeaders(Map<String, String> headers, String token) {
    print('$TAG => [_addTokenToHeaders] => ' + token);
    if (headers == null) headers = Map();

    headers.addEntries([MapEntry(WEATHER_API_KEY, token)]);
    return headers;
  }
}


