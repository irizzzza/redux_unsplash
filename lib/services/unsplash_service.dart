import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:redux_epics_practice/models/dto/image_dto.dart';


class UnsplashService {


  static Future<String> fetchData() async{
    final String url = 'https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0';

    final response = await http.get(url);
    if(response.statusCode >= 400){
      return null;
    }
    print(response.body);
    return response.body;
  }
  static Future<List<ImageDto>> fetchDataToDto() async{
    final String url = 'https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0';

    final response = await http.get(url);
    final responseData = json.decode(response.body) as List<dynamic>;

    List<ImageDto> _listDto = [];

    for( var item in responseData){
      _listDto.add(ImageDto(id: item['id'],color: item['color'],description: item['description'] ,imageUrl: item['urls'],

      ));
    }

    print(_listDto.first.imageUrl);

    return _listDto;
    //return dto;

  }
}