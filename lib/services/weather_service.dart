import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:redux_epics_practice/models/dto/weather_dto.dart';
import 'package:redux_epics_practice/models/weather.dart';
import 'package:redux_epics_practice/res/const.dart';
import 'package:redux_epics_practice/services/http_service.dart';


class WeatherService {



  static Future<WeatherDto> fetchWeather(String cityName) async{


    final response = await HttpService.instance.request(
      'http://api.openweathermap.org/data/2.5/weather?q=$cityName&appid=$WEATHER_API_KEY&units=metric',
    );


    if(response.response == null){
      return null;
    }
    return WeatherDto.fromMap(response.response);

 //  final String url = 'http://api.openweathermap.org/data/2.5/weather?q=$cityName&appid=$WEATHER_API_KEY&units=metric';

 //  final response = await http.get(url);
 //  if(response.statusCode >= 400){
 //    return null;
 //  }

  // final responseData = json.decode(response.body);
  // print(responseData)
  // if(responseData != null){
   //  return WeatherDto.fromMap(responseData);

  // return null;
 }
}