import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics_practice/pages/home_page.dart';
import 'package:redux_epics_practice/pages/weather/weather_page.dart';
import 'package:redux_epics_practice/store/appstate/appstate.dart';

class MyApp extends StatelessWidget {

  final Store<AppState> store;

  MyApp(this.store);


  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        home: WeatherPage(),
      ),
    );
  }
}