import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_epics_practice/pages/home_page_viewmodel.dart';
import 'package:redux_epics_practice/res/const.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector(
        converter: HomePageViewModel.fromStore,

        onInitialBuild: (HomePageViewModel viewModel) => viewModel.getImg(),
        builder: (BuildContext ctx, HomePageViewModel viewModel) {
          return Scaffold(
            body: ListView.builder(
              itemCount: viewModel.imagesDt.length,
              itemBuilder: (BuildContext ctx, int index) {
                return Card(
                  child: Image.network(
                    viewModel.imagesDt[index].imageUrl[REGULAR],
                  ),
                );
              },
            ),
          );
        });
  }
}
