import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics_practice/models/dto/image_dto.dart';
import 'package:redux_epics_practice/store/appstate/appstate.dart';
import 'package:redux_epics_practice/store/photo_state/photo_selector.dart';

class HomePageViewModel {

  final List<ImageDto>  imagesDt;
  final void Function() getImg;


  HomePageViewModel({
    @required this.getImg,
    @required this.imagesDt,
  });

  static HomePageViewModel fromStore(Store<AppState> store) {
    return HomePageViewModel(
      imagesDt: PhotoSelector.getImgs(store),
      getImg:  PhotoSelector.getRequest(store),
    );
  }
}
