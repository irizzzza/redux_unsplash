import 'package:flutter/cupertino.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics_practice/models/weather.dart';
import 'package:redux_epics_practice/store/appstate/appstate.dart';
import 'package:redux_epics_practice/store/weather_state/weather_selector.dart';

class WeatherPageViewModel {

  final Weather weather;

  final void Function (String city) searchField;

  WeatherPageViewModel({@required this.weather, this.searchField,});

  static WeatherPageViewModel fromStore(Store<AppState> store) {
    return WeatherPageViewModel(
      weather: WeatherSelector.getCurrentWeather(store),
      searchField: WeatherSelector.searchCity(store)
    );
  }
}