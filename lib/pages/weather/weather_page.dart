import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_epics_practice/pages/weather/weather_page_viewmodel.dart';
import 'package:redux_epics_practice/res/const.dart';
import 'package:redux_epics_practice/store/appstate/appstate.dart';

class WeatherPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: StoreConnector<AppState, WeatherPageViewModel>(
        converter: WeatherPageViewModel.fromStore,
        builder: (BuildContext ctx, WeatherPageViewModel viewModel){
          return Container(
            margin: const EdgeInsets.only(top: 60.0),
            child: Column(
             children: <Widget>[
               Text(viewModel.weather.cityName ?? EMPTY_STRING),
               Row(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: <Widget>[
                   Text(viewModel.weather?.temp?.toString() ?? EMPTY_STRING),
                   Text(viewModel.weather?.temp?.toString() == null ? EMPTY_STRING : CELSIUM),
                 ],
               ),

               Container(
                 width: 200,
                 padding: const EdgeInsets.symmetric(horizontal: 20.0),
                 child: TextField(
                   onSubmitted: (value){
                     viewModel.searchField(value);

                   },
                 ),
               ),
             ],
            ),
          );
        },
      ),
    );
  }
}
