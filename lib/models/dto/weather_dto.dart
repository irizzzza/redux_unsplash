

class WeatherDto {
  final String cityName;
  final Map<String,dynamic> main;
  final Map<String,dynamic> wind;


  WeatherDto({
    this.cityName,
    this.main,
    this.wind,
  });


  factory WeatherDto.fromMap(Map<String, dynamic> map) {
    return new WeatherDto(
      cityName: map['name'] as String,
      wind: map['wind'] as Map<String, dynamic>,
      main: map['main'] as Map<String,dynamic>,
    );
  }
}
