import 'package:flutter/material.dart';

class ImageDto {
  final String id;
  final String color;
  final String description;
  final Map<String, dynamic> imageUrl;
  ImageDto({
    @required this.id,
    @required this.color,
    @required this.description,
    @required this.imageUrl,
  });
// factory ImageDto.fromJson(Map<String, dynamic> json){
//   return ImageDto(
//     id: json['id'],
//     color: json['color'],
//     description: json['description'],
//     imageUrl: json['urls'],
//   );
// }
}