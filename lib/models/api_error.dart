
enum APIErrorType {
  unknownError,
  httpError,
  parsingError,
}

class APIError {
  final int errorCode;
  final String readableError;
  final APIErrorType type;

  APIError({this.readableError, this.errorCode, this.type});

  static APIError getErrorByCode(int code) {
   print('API ERROR CODE: $code');
  }

  static APIError getHttpError(int code, String message) {
    return APIError(readableError: message, errorCode: code, type: APIErrorType.httpError);
  }

  static APIError getParsingError(String message) {
    return APIError(readableError: message, errorCode: 0, type: APIErrorType.parsingError);
  }
}
