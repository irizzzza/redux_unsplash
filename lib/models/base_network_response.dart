import 'package:redux_epics_practice/models/api_error.dart';

class BaseNetworkResponse {
  APIError error;

  Map<String, dynamic> response;

  BaseNetworkResponse({this.error, this.response});
}
