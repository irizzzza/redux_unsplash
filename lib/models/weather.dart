import 'package:redux_epics_practice/models/dto/weather_dto.dart';

class Weather {
  final String cityName;
  //final Map<String,double> main;
  final Map<String,dynamic> wind;
  final int windDeg;
  final int windSpeed;
  final int temp;
  final int deg;

  Weather({
    this.cityName,
    this.windSpeed,
    this.wind,
    this.windDeg,
    this.temp,
    this.deg,
  });


 static Weather fromWeatherDto(WeatherDto dto){

   if(dto == null){
     return null;
   }
    return Weather(
      cityName: dto.cityName,
      wind: dto.wind,
      temp: dto.main['temp'].round(),
      windSpeed: dto.wind['speed'],
    );
  }
}