import 'package:redux/redux.dart';
import 'package:redux_epics_practice/models/weather.dart';
import 'package:redux_epics_practice/store/appstate/appstate.dart';
import 'package:redux_epics_practice/store/weather_state/actions/get_current_weather_action.dart';

class WeatherSelector {


  static void Function(String city) searchCity(Store<AppState> store) {
    return (city) => store.dispatch(GetCurrentWeatherAction(cityName: city));
  }



 static Weather getCurrentWeather(Store<AppState> store) {
    return  store.state.weatherState.weather;
  }
}