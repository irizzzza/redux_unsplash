import 'package:flutter/cupertino.dart';

class GetCurrentWeatherAction {
  final String cityName;

  GetCurrentWeatherAction({
    @required this.cityName,
  });
}
