import 'package:redux_epics_practice/models/weather.dart';

class SaveWeatherAction {
  final Weather weather;

  SaveWeatherAction({this.weather});

}