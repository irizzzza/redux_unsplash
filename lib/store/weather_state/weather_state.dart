import 'dart:collection';

import 'package:redux_epics_practice/models/dto/image_dto.dart';
import 'package:redux_epics_practice/models/weather.dart';
import 'package:redux_epics_practice/redux/action/save_init_data_action.dart';
import 'package:redux_epics_practice/store/appstate/appstate.dart';
import 'package:redux_epics_practice/store/weather_state/actions/get_current_weather_action.dart';
import 'package:redux_epics_practice/store/weather_state/actions/save_weather_action.dart';

import 'actions/show_current_weather_action.dart';





class WeatherState {
  static const String TAG = 'WeatherState';
  final Weather weather;

  WeatherState ({this.weather});

  factory WeatherState.initial() => WeatherState (weather: Weather() );


  WeatherState copyWith({

    Weather weather,
  }) {
    return WeatherState(
      weather: weather ?? this.weather,
    );
  }

  WeatherState reducer(dynamic action) {
    print('$TAG => Action runtimetype => ${action.runtimeType}');
    return Reducer<WeatherState>(
      actions: HashMap.from({
        SaveWeatherAction : (dynamic action) => saveWeather((action as SaveWeatherAction).weather),
      }),

    ).updateState(action, this);
  }

  WeatherState saveWeather(Weather newWeather){

    if(newWeather == null){
      return null;
    }

    return this.copyWith(
      weather: newWeather
    );
  }

// WeatherState saveData(String cityName){


//   return WeatherState();
// }

}