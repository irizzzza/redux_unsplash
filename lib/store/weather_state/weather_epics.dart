import 'package:redux_epics/redux_epics.dart';
import 'package:redux_epics_practice/models/weather.dart';
import 'package:redux_epics_practice/redux/action/empty_action.dart';
import 'package:redux_epics_practice/repositories/weather_repository.dart';
import 'package:redux_epics_practice/store/appstate/appstate.dart';
import 'package:redux_epics_practice/store/weather_state/actions/get_current_weather_action.dart';
import 'package:redux_epics_practice/store/weather_state/actions/save_weather_action.dart';
import 'package:rxdart/rxdart.dart';

class WeatherEpics {
  static final indexEpic = combineEpics<AppState>([
    requestEpic,
    setInitDataEpic,
  ]);

  static Stream<dynamic> requestEpic(
      Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<GetCurrentWeatherAction>().switchMap(
      (action) {
        return Stream.fromFuture(WeatherRepository.instance
                .getWeather(action.cityName)
                .then((Weather weather) {
          if (weather == null) {
            return EmptyAction();
          }

          return SaveWeatherAction(weather: weather);
         // return weather;
        })

            /* UnsplashRepository.instance.getData().then((listDto){
            print('fwf $listDto');
            if (listDto == null){
              return EmptyAction();
            }
            return SetInitDataAction(
                imgs: listDto
            );
          }*/
            );
      },
    );
  }

  static Stream<dynamic> setInitDataEpic(
      Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<EmptyAction>().switchMap((action) {
      return Stream.fromIterable([
       // SaveInitDataAction(images: action.imgs),

      ]);
    });
  }
}
