import 'dart:collection';

import 'package:flutter/material.dart';

import 'package:redux_epics/redux_epics.dart';
import 'package:redux_epics_practice/models/weather.dart';
import 'package:redux_epics_practice/store/photo_state/photo_epics.dart';
import 'package:redux_epics_practice/store/photo_state/photo_state.dart';
import 'package:redux_epics_practice/store/weather_state/weather_epics.dart';
import 'package:redux_epics_practice/store/weather_state/weather_state.dart';

class AppState {

  final PhotoState photoState;
  final WeatherState weatherState;

  AppState({

    @required this.photoState,
    @required this.weatherState,
  });

  static final getAppEpics = combineEpics<AppState>([
    PhotoEpics.indexEpic,
    WeatherEpics.indexEpic,
  ]);

  factory AppState.initial() => AppState(
    photoState: PhotoState.initial(),
    weatherState: WeatherState.initial(),
  );

  static AppState reducer(AppState state, dynamic action) {
    return AppState(
      photoState: state.photoState.reducer(action),
      weatherState: state.weatherState.reducer(action),
    );
  }
}

class Reducer<T> {
  final String TAG = '[Reducer<$T>]';
  HashMap<dynamic, T Function(dynamic)> actions;

  Reducer({
    @required this.actions,
  }) {
    actions.forEach((key, value) {
      if (value == null) throw ('All Functions must be initialize');
    });
  }

  T updateState(dynamic action, T state) {
    if (actions.containsKey(action.runtimeType)) {
      return actions[action.runtimeType](action);
    }
    return state;
  }
}