import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux_epics_practice/redux/action/empty_action.dart';
import 'package:redux_epics_practice/redux/action/get_img_action.dart';
import 'package:redux_epics_practice/redux/action/save_init_data_action.dart';
import 'package:redux_epics_practice/redux/action/set_init_data_action.dart';
import 'package:redux_epics_practice/repositories/unsplash_repository.dart';
import 'package:redux_epics_practice/res/const.dart';
import 'package:redux_epics_practice/store/appstate/appstate.dart';
import 'package:redux_epics_practice/store/photo_state/actions/request_action.dart';
import 'package:redux_epics_practice/store/photo_state/actions/save_data_action.dart';
import 'package:rxdart/rxdart.dart';

class PhotoEpics {
  static final indexEpic = combineEpics<AppState>([
    requestEpic,
    setInitDataEpic,
  ]);
  static Stream<dynamic> requestEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<RequestAction>().switchMap((action) {
      return Stream.fromFuture(
          UnsplashRepository.instance.getData().then((listDto){
            print('fwf $listDto');
            if (listDto == null){
              return EmptyAction();
            }
            return SetInitDataAction(
                imgs: listDto
            );
          })
      );
    }
    );
  }
  static Stream<dynamic> setInitDataEpic(Stream<dynamic> actions, EpicStore<AppState> store) {

    return actions.whereType<SetInitDataAction>().switchMap((action) {
      return Stream.fromIterable([
        SaveInitDataAction(images: action.imgs),
       //  NavigateToAction.pushNamedAndRemoveUntil(HOME_PAGE_ROUTE, (route) => false)
      ]);
    }
    );
  }
}
