import 'dart:collection';

import 'package:redux_epics_practice/models/dto/image_dto.dart';
import 'package:redux_epics_practice/redux/action/save_init_data_action.dart';
import 'package:redux_epics_practice/store/appstate/appstate.dart';

import 'actions/save_data_action.dart';



class PhotoState {
  static const String TAG = 'PhotoState';
  final List<ImageDto> imageUrls;

  PhotoState (this.imageUrls);

  factory PhotoState.initial() => PhotoState ([]);


  PhotoState reducer(dynamic action) {
    print('$TAG => Action runtimetype => ${action.runtimeType}');
    return Reducer<PhotoState>(
      actions: HashMap.from({
        SaveInitDataAction: (dynamic action) => saveData((action as SaveInitDataAction).images),
      }),
    ).updateState(action, this);
  }

  PhotoState saveData(List<ImageDto> data){
    List<ImageDto> dtos = [];
    print('zzzzz $data');
    data.forEach((el) {
      dtos.add( el);
    });
    return PhotoState(dtos);
  }

}