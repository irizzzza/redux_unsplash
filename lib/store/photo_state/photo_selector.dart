import 'package:redux/redux.dart';
import 'package:redux_epics_practice/models/dto/image_dto.dart';
import 'package:redux_epics_practice/store/appstate/appstate.dart';

import 'actions/request_action.dart';


class PhotoSelector {


  static void Function() getRequest(Store<AppState> store) {
    return () => store.dispatch(RequestAction());
  }

  static List<ImageDto> getImgs(Store<AppState> store) {
    return  store.state.photoState.imageUrls;
  }
}