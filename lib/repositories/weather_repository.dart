import 'package:redux_epics_practice/models/weather.dart';
import 'package:redux_epics_practice/services/weather_service.dart';

class WeatherRepository {
  WeatherRepository._privateConstructor();

  static final WeatherRepository instance =
      WeatherRepository._privateConstructor();

  Future<Weather> getWeather(String cityName) async {
    return Weather.fromWeatherDto(
      await WeatherService.fetchWeather(cityName),
    );
  }
}
