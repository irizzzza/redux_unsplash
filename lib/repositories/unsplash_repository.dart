
import 'package:redux_epics_practice/models/dto/image_dto.dart';
import 'package:redux_epics_practice/services/unsplash_service.dart';

class UnsplashRepository {
  UnsplashRepository._privateConstructor();
  static final UnsplashRepository instance = UnsplashRepository._privateConstructor();
  Future<List<ImageDto>> getData() async {
    return UnsplashService.fetchDataToDto();
  }
}