import 'package:flutter/material.dart';
import 'package:redux_epics_practice/models/dto/image_dto.dart';

class SetInitDataAction  {
  final List<ImageDto>  imgs;

  SetInitDataAction({@required this.imgs});
}
