import 'package:flutter/cupertino.dart';
import 'package:redux_epics_practice/models/dto/image_dto.dart';

class SaveInitDataAction {
  final List<ImageDto> images;
  SaveInitDataAction({@required this.images,});
}